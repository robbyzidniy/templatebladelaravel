<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/Register', function () {
    return view('welcome');
});


Route::get('/index', 'HomeController@home');
Route::get('/form', 'AuthController@register');
Route::get('/welcomee', 'AuthController@welcome');

Route::post('/index', 'AuthController@register');
Route::post('/welcomee', 'AuthController@welcome');

Route::get ('/master', function (){
    return view ('adminlte/master');
});

Route::get ('/table', function (){
    return view ('adminlte/partials/table');
});

Route::get ('/tables-data', function (){
    return view ('adminlte/partials/tables-data');
});